<?php

namespace Tests\Unit;

use App\Constants\CurrencyConstants;
use App\Models\Currency;
use App\Repositories\CurrencyRepositories;
use App\Services\CurrencyService;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CurrencyServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    /**
     * 換匯計算測試(入門)
     *
     * @return void
     */
    public function testTransformWithRate()
    {
        $rate = '12.345';
        $amount = '100.00000';
        $expected = '1,234.50';

        $service = app(CurrencyService::class);
        $result = $service->transformWithRate($rate, $amount);

        $this->assertEquals($result, $expected);
    }

    /**
     * 換匯計算測試(mock入門)
     *
     * @return void
     */
    public function testAmountTransformWithCustomRate()
    {
        $rate = '12.345';
        $params = [
            'source' => CurrencyConstants::TWD,
            'target' => CurrencyConstants::USD,
            'amount' => '100',
        ];
        $expected = '1,234.50';

        $repositorise = Mockery::mock(CurrencyRepositories::class, function (MockInterface $mock) use ($rate) {
            $mock->shouldReceive('getRate')->once()->andReturn($rate);
        });

        $service = new CurrencyService($repositorise);
        $result = $service->amountTransform($params);

        $this->assertEquals($result, $expected);
    }

    /**
     * 換匯計算測試(with factory)
     *
     * @return void
     */
    public function testAmountTransformWithFactory()
    {
        $model = Currency::factory(['source' => CurrencyConstants::TWD])->create();
        $amount = '100';
        $params = [
            'source' => CurrencyConstants::TWD,
            'target' => $model->target,
            'amount' => 100,
        ];
        $expected = bcmul($amount, $model->rate, 3);
        $expected = number_format($expected, 2, '.', ',');

        $service = app(CurrencyService::class);
        $result = $service->amountTransform($params);

        $this->assertEquals($result, $expected);
    }

    /**
     * 變動匯率測試
     *
     * @dataProvider parameterWithRateProvider
     * @param array $params 輸入參數
     * @param string $rate 變動匯率
     * @param string $expected 預期金額
     * @return void
     */
    public function testAmountTransformWithChangeRate(array $params, string $rate, string $expected)
    {
        $this->instance(
            CurrencyRepositories::class,
            Mockery::mock(CurrencyRepositories::class, function (MockInterface $mock) use ($rate) {
                $mock->shouldReceive('getRate')->once()->andReturn($rate);
            })
        );

        $service = app(CurrencyService::class);
        $result = $service->amountTransform($params);

        $this->assertEquals($result, $expected);
    }

    /**
     * 換匯計算測試
     *
     * @dataProvider parameterProvider
     * @param array $params 輸入參數
     * @param string $expected 預期金額
     * @return void
     */
    public function testAmountTransform(array $params, string $expected)
    {
        $service = app(CurrencyService::class);
        $result = $service->amountTransform($params);

        $this->assertEquals($result, $expected);
    }

    /**
     * 變動匯率測試資料
     */
    public function parameterWithRateProvider(): array
    {
        return [
            '極大匯率(12345.67899)(100元)' => [
                [
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
                'amount' => 100,
                ],
                '12345.67899',
                '1,234,567.90',
            ],
            '極小匯率(0.000123)(10元)' => [
                [
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
                'amount' => 10,
                ],
                '0.000123',
                '0.00',
            ],
        ];
    }

    /**
     * 正常測試資料
     */
    public function parameterProvider(): array
    {
        return [
            '4捨5入測試(台幣換美金)' => [
                [
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
                'amount' => 10,
                ],
                '0.33',
            ],
            '0元測試(台幣換美金)' => [
                [
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
                'amount' => 0,
                ],
                '0.00',
            ],
            '非整數(台幣換美金)' => [
                [
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
                'amount' => 123456.78,
                ],
                '4,050.62',
            ],
            '台幣換日幣' => [
                [
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::JPY,
                'amount' => 100,
                ],
                '366.90',
            ],
            '台幣換美金' => [
                [
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
                'amount' => 100,
                ],
                '3.28',
            ],
            '台幣換台幣' => [
                [
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::TWD,
                'amount' => 100,
                ],
                '100.00',
            ],

            '日幣換台幣' => [
                [
                'source' => CurrencyConstants::JPY,
                'target' => CurrencyConstants::TWD,
                'amount' => 100,
                ],
                '26.96',
            ],
            '日幣換美金' => [
                [
                'source' => CurrencyConstants::JPY,
                'target' => CurrencyConstants::USD,
                'amount' => 100,
                ],
                '0.89',
            ],
            '日幣換日幣' => [
                [
                'source' => CurrencyConstants::JPY,
                'target' => CurrencyConstants::JPY,
                'amount' => 100,
                ],
                '100.00',
            ],

            '美金換台幣(含千分位符號)' => [
                [
                'source' => CurrencyConstants::USD,
                'target' => CurrencyConstants::TWD,
                'amount' => 100,
                ],
                '3,044.40',
            ],
            '美金換日幣(含千分位符號)' => [
                [
                'source' => CurrencyConstants::USD,
                'target' => CurrencyConstants::JPY,
                'amount' => 100,
                ],
                '11,180.10',
            ],
            '美金換美金' => [
                [
                'source' => CurrencyConstants::USD,
                'target' => CurrencyConstants::USD,
                'amount' => 100,
                ],
                '100.00',
            ],
        ];
    }
}
