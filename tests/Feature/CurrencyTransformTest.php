<?php

namespace Tests\Feature;

use App\Constants\AppConstants;
use App\Constants\CurrencyConstants;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CurrencyTransformTest extends TestCase
{
    /**
     * 測試目標API
     */
    protected function getPath(): string
    {
        return '/api/currency-transform';
    }

    /**
     * 換匯API測試
     *
     * @param array $params 搜尋參數
     * @dataProvider parameterProvider
     * @return void
     */
    public function testCurrencyTransformApi($params)
    {
        $response = $this->get($this->getPath() . '?' . http_build_query($params));

        // 資料結構
        $expectedStructure = [
            'status',
            'message',
            'data' => [
                'amount'
            ],
        ];
        $response->assertStatus(200)
            ->assertJsonStructure($expectedStructure); // 測試資料結構
    }

    /**
     * 測試戶口列表API: 錯誤參數
     *
     * @param array $params
     * @dataProvider wrongParameterProvider
     * @return void
     */
    public function testWrongParameter($params)
    {
        $response = $this->get($this->getPath() . '?' . http_build_query($params));

        // var_dump($response->getContent());
        $response->assertUnprocessable()
            ->assertJson(['status' => AppConstants::FALL_VALIDATION]);
    }

    public function parameterProvider(): array
    {
        return [
            '不同幣種換匯' => [[
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
                'amount' => 1000,
            ]],
            '同幣種換匯' => [[
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::TWD,
                'amount' => 1000,
            ]],
        ];
    }

    public function wrongParameterProvider(): array
    {
        return [
            '未設定當前幣種' => [[
                'target' => CurrencyConstants::USD,
                'amount' => 1000,
            ]],
            '未設定目標幣種' => [[
                'source' => CurrencyConstants::USD,
                'amount' => 1000,
            ]],
            '未設定金額' => [[
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
            ]],
            '幣種為空' => [[
                'source' => '',
                'target' => CurrencyConstants::USD,
                'amount' => 1000,
            ]],
            '金額為空' => [[
                'source' => CurrencyConstants::TWD,
                'target' => CurrencyConstants::USD,
                'amount' => '',
            ]],
            '不正確的幣種' => [[
                'source' => 'USTD',
                'target' => CurrencyConstants::USD,
                'amount' => 1000,
            ]],
            '非正確金額' => [[
                'source' => 'USTD',
                'target' => CurrencyConstants::USD,
                'amount' => 'error amount',
            ]],
        ];
    }
}
