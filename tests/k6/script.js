import http from 'k6/http';
import { check, sleep } from 'k6';

// docker-compose run --rm k6 run /scripts/script.js
// k6 rum --vus 10 --duration 30s
export const options = {
  vus: 10, // "用戶"(並行數)
  duration: '10s', // 進行時間
};

export default function () {
  var host = 'http://laravel8.test';
  // var host = 'http://laravel8.octane';
  const res = http.get(host + '/api/currency-transform?source=TWD&target=USD&amount=10');
  check(res, { "status was 200": (r) => r.status == 200 });
  // sleep(1);
}
