import http from 'k6/http';
import { check, sleep } from 'k6';


// 負載分階段變化
export const options = {
  stages: [
    { duration: '3s', target: 20 },
    { duration: '1s', target: 100 },
    { duration: '1m', target: 100 },
    { duration: '5s', target: 0 },
  ],
};

export default function () {
  const res = http.get('http://laravel8.test/api/currency-transform?source=TWD&target=USD&amount=10');
  check(res, { "status was 200": (r) => r.status == 200 });
  sleep(1);
}
