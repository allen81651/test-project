<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CurrencyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'source' => $this->faker->regexify('[A-Z]{3}'),
            'target' => $this->faker->regexify('[A-Z]{3}'),
            'rate' => $this->faker->randomFloat('2', 1, 100),
        ];
    }
}
