<?php

namespace Database\Seeders;

use App\Constants\CurrencyConstants;
use App\Models\Currency;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table((new Currency())->getTable())->truncate();

        $data = [
            ['source' => CurrencyConstants::TWD, 'target' => CurrencyConstants::TWD, 'rate' => '1'],
            ['source' => CurrencyConstants::TWD, 'target' => CurrencyConstants::JPY, 'rate' => '3.669'],
            ['source' => CurrencyConstants::TWD, 'target' => CurrencyConstants::USD, 'rate' => '0.03281'],

            ['source' => CurrencyConstants::JPY, 'target' => CurrencyConstants::TWD, 'rate' => '0.26956'],
            ['source' => CurrencyConstants::JPY, 'target' => CurrencyConstants::JPY, 'rate' => '1'],
            ['source' => CurrencyConstants::JPY, 'target' => CurrencyConstants::USD, 'rate' => '0.00885'],

            ['source' => CurrencyConstants::USD, 'target' => CurrencyConstants::TWD, 'rate' => '30.444'],
            ['source' => CurrencyConstants::USD, 'target' => CurrencyConstants::JPY, 'rate' => '111.801'],
            ['source' => CurrencyConstants::USD, 'target' => CurrencyConstants::USD, 'rate' => '1'],
        ];

        Currency::insert($data);
    }
}
