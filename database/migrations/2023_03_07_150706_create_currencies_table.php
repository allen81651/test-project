<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->id();
            $table->string('source', 5)->comment('輸入幣種代碼');
            $table->string('target', 5)->comment('輸入幣種代碼');
            $table->decimal('rate', 12, 6, true)->comment('匯率');
            $table->timestamps();

            $table->unique(['source', 'target']);
        });

        DB::statement("ALTER TABLE `currencies` comment '貨幣匯率表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
