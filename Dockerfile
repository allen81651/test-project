FROM php:8.1.3

RUN apt-get update && apt-get install -y wget git libssl-dev libzip-dev zip libpng-dev libjpeg-dev

RUN pecl install swoole \
    && docker-php-ext-enable swoole

RUN pecl install zip

RUN docker-php-ext-configure gd --enable-gd --with-jpeg

RUN docker-php-ext-install -j$(nproc) zip bcmath ctype pdo_mysql pcntl gd

RUN wget https://github.com/composer/composer/releases/download/2.2.6/composer.phar
RUN chmod u+x composer.phar

RUN mv composer.phar /usr/local/bin/composer

# 安裝ps指令
RUN apt-get update && apt-get install -y procps && rm -rf /var/lib/apt/lists/*

WORKDIR /laravel
COPY . .

RUN mkdir -p ./storage/logs \
    && chmod -R 777 ./storage/logs

RUN /usr/local/bin/composer install
# RUN /usr/local/bin/composer install --no-dev

CMD ["php", "artisan", "octane:start", "--host=0.0.0.0"]
