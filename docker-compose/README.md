使用`docker-compose`建立本地開發環境

#### 系統版本

專案:
- nginx:alpine
- php 8.1
- MySQL 5.7
- redis:latest

本地:
- docker
- git
- php8.1 & composer

#### 建立docker環境

1. *install docker
2. 進入本專案主目錄(docker-compose.yml檔所在地)
3. `cp .env.example .env`
4. 修改`.env`檔參數(產品專案所在路徑、DB設定、docker資料&log路徑等)
5. `docker-compose up -d`

#### host 設定

開啟檔案路徑C:\Windows\System32\drivers\etc\hosts，新增host

> *10.40.3.228 為host.docker.internal IP，
> hosts設127.0.0.1在container內會導向container自己，需改成這個

#### Load Balance
- 負載均衡，設定檔見`upstream.conf`
- 啟動指令範例:
	`docker-compose up -d --scale php-fpm=2`
	*`--scale [services=num]` 設定某服務要啟動幾台

#### k6壓測
- 測試檔放在 /tests/k6
- 運行測試指令範例: `docker-compose run --rm k6 run /scripts/script.js`

#### grafana
- http://localhost:3000/
- k6面板
https://blog.knoldus.com/k6-results-with-influxdb-and-grafana/
