<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CurrencyTransformRequest;
use App\Services\CurrencyService;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * 換匯計算
     * 
     * @param CurrencyTransformRequest $request
     * @param CurrencyService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function transform(CurrencyTransformRequest $request, CurrencyService $service)
    {
        $params = $request->validated();
        $result = $service->amountTransform($params);

        return $this->success(['amount' => $result]);
    }
}
