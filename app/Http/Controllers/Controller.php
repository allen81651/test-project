<?php

namespace App\Http\Controllers;

use App\Constants\AppConstants;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 制式化回應
     *
     * @param int $status 狀態
     * @param string $message 訊息
     * @param array $data 回傳資料
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(int $status, string $message, array $data)
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    /**
     * 成功回傳
     *
     * @param array $data 回傳資料
     * @return \Illuminate\Http\JsonResponse
     */
    public function success(array $data = [])
    {
        return $this->response(AppConstants::SUCCESS, '成功', $data);
    }
}
