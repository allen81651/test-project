<?php

namespace App\Http\Requests;

use App\Constants\AppConstants;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseRequest extends FormRequest
{
    /**
     * 複寫錯誤回傳
     */
    protected function failedValidation(Validator $validator)
    {
        $response = response()->json(
            [
                'status' => AppConstants::FALL_VALIDATION,
                'message' => '驗證失敗',
                'data' => $validator->errors(),
            ],
            AppConstants::FALL_VALIDATION
        );

        throw new HttpResponseException($response);
    }
}
