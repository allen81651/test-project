<?php

namespace App\Http\Requests;

use App\Constants\CurrencyConstants;
use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CurrencyTransformRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currencyList = [
            CurrencyConstants::TWD,
            CurrencyConstants::JPY,
            CurrencyConstants::USD,
        ];
        return [
            'source' => ['required', 'string', Rule::in($currencyList)],
            'target' => ['required', 'string', Rule::in($currencyList)],
            'amount' => ['required', 'regex:/^\d+(\.\d{1,2})?$/'],
        ];
    }
}
