<?php

namespace App\Services;

use App\Repositories\CurrencyRepositories;

/**
 * 幣種服務
 */
class CurrencyService
{
    /** @var CurrencyRepositories 幣種設定資料 */
    protected $currencyRepositories;

    public function __construct(CurrencyRepositories $currencyRepositories)
    {
        $this->currencyRepositories = $currencyRepositories;
    }

    /**
     * 換匯計算
     *
     * @param array $params 輸入參數
     * @return string 轉換後數值
     */
    public function amountTransform(array $params)
    {
        $rate = $this->currencyRepositories->getRate($params['source'], $params['target']);

        return $this->transformWithRate($rate, $params['amount']);
    }

    /**
     * 輸入匯率計算金額
     *
     * @param string $rate 匯率
     * @param string $amount 金額
     * @return string 格式化後金額
     */
    public function transformWithRate($rate, $amount)
    {
        $result = bcmul($amount, $rate, 3);
        $result = number_format($result, 2, '.', ',');

        return $result;
    }
}
