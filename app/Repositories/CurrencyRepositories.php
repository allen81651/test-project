<?php

namespace App\Repositories;

use App\Constants\CurrencyConstants;
use App\Models\Currency;

/**
 * 貨幣匯率表
 */
class CurrencyRepositories
{
    /** @var Currency 貨幣匯率表 */
    protected $model;

    public function __construct(Currency $currencyModel)
    {
        $this->model = $currencyModel;
    }

    /**
     * 取得匯率
     *
     * @param string $source 來源幣別
     * @param string $target 目標幣別
     * @return string 匯率
     */
    public function getRate($source, $target)
    {
        $data = $this->model->query()
            ->select(['rate'])
            ->where(['source' => $source, 'target' => $target])
            ->first();

        if (empty($data->rate)) {
            // todo: error handle
            return 0;
        }

        return $data->rate;
    }
}
