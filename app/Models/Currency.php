<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 貨幣匯率表
 *
 * @property int $id
 * @property string $source 輸入幣種代碼
 * @property string $target 輸入幣種代碼
 * @property string $rate 匯率 decimal(12,6)
 * @property string $created_at 創建時間
 * @property string $updated_at 修改時間
 */
class Currency extends Model
{
    use HasFactory;
}
