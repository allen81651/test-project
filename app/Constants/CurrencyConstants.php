<?php

namespace App\Constants;

/**
 * 幣種常數
 */
class CurrencyConstants
{
    /** @var string 新台幣 */
    const TWD = 'TWD';
    /** @var string 日幣 */
    const JPY = 'JPY';
    /** @var string 美金 */
    const USD = 'USD';
}
