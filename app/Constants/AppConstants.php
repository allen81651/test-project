<?php

namespace App\Constants;

/**
 * 系統常數
 */
class AppConstants
{
    /** @var string 成功 */
    const SUCCESS = 200;
    /** @var string 驗證失敗 */
    const FALL_VALIDATION = 422;
}
