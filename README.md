# 練習用專案
- php 8.1
- laravel 8 框架
- OpenApi 3.0 文檔
- 含單元測試 & API測試

## 專案建置
**啟動docker容器**
```bash
cd docker-compose
docker-compose up -d nginx mysql
```
**專案安裝**
```bash
cp .env.example .env
cp .env.example .env.testing
composer install
php artisan key:generate
```

## OpenApi文件
- yaml格式
- 使用Redoc套件
- url: /redoc-static.html
- 打包好的文檔路徑: ./public/redoc-static.html
- 文檔路徑: ./resources/spec/swagger.yaml
###### 打包指令:
```bash
yarn spec-build
```

## 測試指令
```bash
php artisan test --filter CurrencyTransformTest
php artisan test --filter CurrencyServiceTest
```

## docker (laravel-octane 獨立運行版)
- compose file: /docker-compose.yml
- docker file: /Dockerfile
#### 將程式碼打包進image
```bash
docker-compose build laravel-octane-build
```
#### 運行打包好的image
url: 0.0.0.0:8000
```bash
docker-compose up -d laravel-octane
```